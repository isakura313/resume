import {createApp} from 'vue'
import './style.css'
import App from './App.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/css/style.css'
import en from './locales/en.js'
import ru from './locales/ru.js'
import {createI18n} from 'vue-i18n'

const messages = {
    en,
    ru
}

const i18n = createI18n({
    locale: navigator.language, // set locale
    fallbackLocale: 'en', // set fallback locale
    messages

})

createApp(App).use(i18n).mount('#app')
